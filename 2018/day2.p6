use v6;
use lib <lib>;

use Utils;

sub day2-pt1(@input) {
    my %count = "2" => 0, "3" => 0;
    for @input -> $line {
        my $bag = bag $line.comb;
        %count<2>++ if $bag.first(*.value == 2);
        %count<3>++ if $bag.first(*.value == 3);
    }
    say %count<2> * %count<3>;
}

sub wolo(@params) {
    my \s = @params[0];
    my \t = @params[1];
    my $ld = levenshtein(s, t);
    say "Got $ld for {s} and {t}";
    return (s, t, $ld);
}

sub wole(@params) {
    my $s = @params[0];
    my $t = @params[1];

    my $cmp = ($s.comb Z $t.comb).grep({ $_[0] ne $_[1] }).elems;
    if $cmp == 1 {
        say "Got $cmp for $s and $t"
    }
    return $s, $t, $cmp
}

sub day2-pt2(@input) {
    my ($a, $b, $c) = @input.combinations(2).race.map(&wole).first(-> @vals { @vals[2] == 1 });
    say $a.comb Z $b.comb;
    say ($a.comb Z $b.comb).grep({ $_[0] eq $_[1] }).map(*.first).join
}

my @input = parse-input;
day2-pt1(@input);
day2-pt2(@input);
