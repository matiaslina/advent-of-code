#!/usr/bin/env perl6
use v6;
use lib 'lib';
use AdventOfCode;

sub MAIN($day) {
    given $day {
        when 1 {
            my $text = 'inputs/day1'.IO.slurp;
            say "Day01 a: " ~ day01($text);
            say "Day01 b: " ~ day01b($text);
        }
        when 2 {
            my @spreadsheet;
            for 'inputs/day2'.IO.lines -> $line {
                @spreadsheet.push($line.split(/\s+/).map(*.Int));
            }
            say "Day02 a: " ~ checksum(@spreadsheet);
            say "Day02 b: " ~ evenly-divisible-values(@spreadsheet);

        }
        when 3 {
            say "Spiral memory for 12: " ~ spiral-memory(12);
        }
        default { say "uninplemented"; }
    }
}
