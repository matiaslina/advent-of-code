use v6;
use Test;
use AdventOfCode;

plan 5;

is spiral-memory(1), 0;
is spiral-memory(9), 2;
is spiral-memory(12), 3;
is spiral-memory(23), 2;
is spiral-memory(1024), 31;
