#!/usr/bin/env raku

use v6;

constant \EXAMPLE = q:to/END/;
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
END

sub solve($input, Int $step-right, Int $step-down --> Int) {
    # Parse it
    my @m = $input.split("\n").grep(*.chars > 0).map(*.comb.Array);
    my $x = 0;
    my $y = 0;
    my $trees = 0;
    my $width = @m[0].elems;

    while $y < @m.elems {
        $trees++ if @m[$y][$x] eq '#';

        $y += $step-down;
        $x = ($x + $step-right) % $width;
    }
    $trees
}

multi sub MAIN('part1', $file) {
    say solve(slurp($file), 3, 1);
}

multi sub MAIN('part2', $file) {
    my $input = slurp $file;
    my &f = &solve.assuming($input);
    say (
        f(1, 1),
        f(3, 1),
        f(5, 1),
        f(7, 1),
        f(1, 2)
    ).reduce: &[*]
}

multi sub MAIN('test') {
    use Test;
    subtest 'Part 1', {
        is solve(EXAMPLE, 3, 1), 7, 'Example';
    }
}
